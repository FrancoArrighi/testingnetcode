using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerNetwork : NetworkBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform spawnPos;
    [SerializeField] private float velocity;

    private NetworkVariable<CustomData> playerData = new NetworkVariable<CustomData>(new CustomData     // tiene que ser un valor de tipo no de referencia
    {
        _playerNum = 5,
        _dead = false,

    }, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner); // si o si inicializar5lo aca o en un constructor 


    public struct CustomData : INetworkSerializable // aca guardo los datos que quiero que se transmitan, lo menos posible 
    {
        public int _playerNum;
        public bool _dead;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref _playerNum);
            serializer.SerializeValue(ref _dead);
        }
    } 

    public override void OnNetworkSpawn() // cuando se conecta al servidor 
    {
        playerData.OnValueChanged += (CustomData previousValue, CustomData newValue) => // si cambian los datos que se ejecute el log 
        {
            Debug.Log(OwnerClientId + ": " + newValue._playerNum);
        };
    }

    void Update()
    {
        if (!IsOwner) return; // para que solo se mueva mi personaje del cliente 
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            playerData.Value = new CustomData // cambio el valor de los datos y se activa el evento que imprime un log 
            {
                _playerNum = Random.Range(0, 10),
                _dead = true,
            };
            ShootBulletServerRpc();
        }

        Camera cam = Camera.main;
        Vector3 mousePos = cam.WorldToViewportPoint(transform.position);
        Vector3 mouseOnScreen = (Vector2)cam.ScreenToViewportPoint(Input.mousePosition);

        Vector3 lookPos = mouseOnScreen - mousePos;

        float angle;      
        angle = Mathf.Atan2(lookPos.x, lookPos.y) * Mathf.Rad2Deg;     
        transform.rotation = Quaternion.Euler(new Vector3(0, angle, 0));
    }

    private void FixedUpdate()
    {
        float horizontalInput;
        float verticalInput;

        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        Vector3 movementDirection = new Vector3(horizontalInput, 0, verticalInput);
        movementDirection.Normalize();

        transform.Translate(movementDirection * velocity * Time.deltaTime, Space.World);
    }

    [ServerRpc]
    private void ShootBulletServerRpc() // cada cliente le pide al server para hacer sus acciones 
    {
        ShootClientRpc();
        // el server ejecuta la peticion en cada uno de los clientes 
    }
    [ClientRpc]
    private void ShootClientRpc()
    {
        Shoot(); // se va a ver el disparo en todos los clientes
    }
    private void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, spawnPos.position, spawnPos.transform.rotation) as GameObject;
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.AddForce(rb.transform.forward * 500);
        Destroy(bullet, 2.0f);
    }
}
